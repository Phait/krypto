import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class Aufgabe1_4 {
	public static void main(String[] args) throws IOException {
		BufferedReader buf = new BufferedReader(new FileReader("geheim2.txt"));

		Analyser analyser = new Analyser(buf.readLine());

		analyser.decrypt();

		buf.close();
	}
}
