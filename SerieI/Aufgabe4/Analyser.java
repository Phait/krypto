import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.LinkedList;

/**
 * Diese Klasse analysiert und entschluesselt allgemeine monoalphabetisch verschl�sselte Texte
 * @author Sonja Doerband
 *
 */
public class Analyser {
	String secret;
	String encrypted;
	Map<Character, Integer> letterFrequency;
	LinkedList<String> switches;


	public Analyser(String secret) {
		this.secret = secret;
		this.encrypted = secret;
		letterFrequency = new HashMap<Character, Integer>();
		switches = new LinkedList<String>();
	}

	/**
	 * Diese Methode analysiert die Buchstabenhaeufigkeit aus dem Geheimtext
	 */
	private void analyse() {
		for (int i = 0; i < secret.length(); i++) {
			Character key = secret.charAt(i);

			if (letterFrequency.containsKey(key)) {
				letterFrequency.put(key, letterFrequency.get(key) + 1);
			} else {
				letterFrequency.put(key, 1);
			}
		}
	}

//	/**
//	 * Diese Methode tauscht auf Basis der Buchstabenhaeufigkeit die Buchstaben nach Haeufigkeitsvorkommen in der deutschen Sprache aus
//	 *
//	 */
//	private void setLetters() {
//		String frequency = "ENISRATDHULCGMOWBFKZPVJYXQ"; // Deutsche Buchstabenhaeufigkeit
//		char[] charArray = frequency.toCharArray();
//
//		Map<Character, Integer> letters = new HashMap<Character, Integer>(letterFrequency);
//		int[] indexArray;
//		int temp = 0;
//		char letter = 0;
//		int value = 0;
//		int position = 0;
//
//		while (!letters.isEmpty()) {
//			for (Entry<Character, Integer> e : letters.entrySet()) {
//				value = (int) e.getValue();
//
//				if (temp < value) {
//					temp = value;
//					letter = (Character) e.getKey();
//				}
//			}
//
//			letters.remove(letter);
//			temp = 0;
//			indexArray = getIndexes(letter);
//			encrypted = swapChars(letter, charArray[position], indexArray);
//			position++;
//		}
//
//	}

	/**
	 * Diese Methode hilft dem Anwender beim entschluesseln des Textes, durch manuelle eingabe der Buchstaben die getauscht werden sollen
	 * @throws IOException
	 */
	public void decrypt() throws IOException {
		FileWriter filewriter = new FileWriter("Aufgabe4Entschluesselung.txt");				// Alle Analysen und Tauschoperationen werden in dieser Textdatei festgehalten
		BufferedWriter writer = new BufferedWriter(filewriter);
		Scanner scannie = new Scanner(System.in);
		int[] indexes = null;
		char oldChar, newChar, encrypt;

		analyse();

		System.out.println("Der verschl�sselte Text lautet: ");
		System.out.println(secret + "\n");
		System.out.println("Im Text wurde folgende Buchstabenverteilungen festgestellt: ");

		System.out.println(letterFrequency.toString() + "\n");

		//setLetters();

		System.out.println("Entschl�sselt auf Basis der Buchstabenverteilung: ");
		System.out.println(encrypted + "\n");

		writer.write("Der verschl�sselte Text lautet: \n");
		writer.write(secret + "\n\n");
		writer.write("Im Text wurde folgende Buchstabenverteilungen festgestellt: \n");
		writer.write(letterFrequency.toString() + "\n\n");
		writer.write("Entschl�sselt auf Basis der Buchstabenverteilung: \n");
		writer.write(encrypted + "\n\n");

		do {
			scannie.reset();
			System.out.println("M�chtest du den Text entschl�sseln? --> J / N");
			encrypt = scannie.nextLine().toUpperCase().toCharArray()[0];
			if (encrypt == 'J') {
				System.out.println("Gebe nun die Buchstaben ein die du miteinander tauschen m�chtest.");
				System.out.println("1. Buchstabe der ersetzt werden soll: ");
				oldChar = scannie.nextLine().toUpperCase().toCharArray()[0];
				System.out.println("2. Buchstabe der eingef�gt werden soll: ");
				newChar = scannie.nextLine().toUpperCase().toCharArray()[0];
				switches.add(oldChar + "->" + newChar);
				indexes = getIndexes(oldChar);
				encrypted = swapChars(oldChar, newChar, indexes);
				System.out.println("Der Text lautet nun: ");
				System.out.println(encrypted);
				System.out.println("Getauschte Paare: ");
				System.out.println(switches);

				writer.write("1. Buchstabe: " + oldChar + "\n");
				writer.write("2. Buchstabe: " + newChar + "\n");
				writer.write(encrypted + "\n\n");
			}
		} while (encrypt != 'N');

		scannie.close();
		writer.close();
	}

	/**
	 * Diese Methode tauscht einen Buchstaben in dem Geheimtext durch einen anderen aus
	 * @param oldChar Der Buchstaben der ausgetauscht werden soll
	 * @param newChar Der Buchstabe der eingetragen werden soll
	 * @param indexes enthaelt die Positionen wo der Buchstabe sich befindet, der getauscht werden soll
	 * @return Gibt den neuen Text zurueck der sich durch den Tausch ergeben hat
	 */
	private String swapChars(char oldChar, char newChar, int[] indexes) {
		StringBuilder string = new StringBuilder();
		string = string.append(encrypted);

		for (int i = 0; i < indexes.length; i++) {
			if (encrypted.charAt(indexes[i]) == oldChar)
				string.setCharAt(indexes[i], newChar);
		}

		return string.toString();
	}

	/**
	 * Diese Methode ermittels wo sich ein Buchstabe im Geheimtext befindet
	 * @param letter Der gesuchte Buchstabe
	 * @return Gibt ein int Array zurueck in dem die Stellen eingetragen sind wo der Buchstaben sich befindet
	 */
	private int[] getIndexes(char letter) {
		List<Integer> indexesList = new ArrayList<>();
		int[] indexes;

		for (int i = 0; i < secret.length(); i++) {
			if (secret.charAt(i) == letter) {
				indexesList.add(i);
			}
		}

		indexes = new int[indexesList.size()];
		for (int i = 0; i < indexesList.size(); i++)
			indexes[i] = indexesList.get(i);

		return indexes;
	}

}
