package Aufg3;

import java.io.*;

class KappaMain{
  public static void main(String[] args) throws Exception{
    if(args.length != 2){
      System.err.println("Wrong number of arguments <Inputfile> <Outputfile>");
      System.exit(1);
    }
    String secretText = new BufferedReader(new FileReader(args[0])).readLine();
    BufferedWriter bw = new BufferedWriter(new FileWriter(new File(args[1])));
    bw.write(Kappa.decrypt(secretText));
    bw.close();
  }
}
