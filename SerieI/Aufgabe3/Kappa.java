package Aufg3;

public class Kappa{
  /**
  * automatically decrypt given enycrypted String
  */
  public static String decrypt(String secret){
    StringBuilder sb = new StringBuilder();
    int keyLength = getKeyLength(secret);
    String keyWord = getKeyWord(secret, keyLength);

    sb.append("GeheimText:\n" + secret + "\n");
    sb.append("keyLength: " + keyLength + "\n");
    sb.append("keyWord: "  + keyWord + "\n");
    sb.append("KlarText:\n");

    int curpos = 0;
    for(int i = 0; i<secret.length(); i++){
      //secret.charAt(i)-(keyWord.charAt(curpos)-'A')
      sb.append((char)(secret.charAt(i)-(keyWord.charAt(curpos)-'A') < 'A'
        ? secret.charAt(i)-(keyWord.charAt(curpos)-'A')+26
        : secret.charAt(i)-(keyWord.charAt(curpos)-'A')));
      curpos = (curpos+1)%6;
    }

    return sb.toString();
  }

  /**
  * returns KeyLength based on results of Friedmann-Kappa test
  */
  private static int getKeyLength(String secret){
      int bestshift = 0;
      double curMax = 0.0;
      for(int i = 1; i < secret.length()-1; i++){
        if(getKappa(secret,stringShift(secret,i)) > curMax){
          bestshift = i;
          curMax = getKappa(secret,stringShift(secret,i));
        }
      }
      return bestshift;
  }

  /**
  * executes Friedman kappa test
  */
  private static double getKappa(String a, String b){
    double ret = 0.0;
    for(int i = 0; i < a.length(); i++){
      ret += (a.charAt(i)==b.charAt(i) ? 1 : 0);
    }
    return (ret / (double) a.length());
  }

  /**
  * shifts String by given offset; Uses wraparound
  */
  private static String stringShift(String s, int shift){
    char[] ret = new char[s.length()];
    for(int i = 0; i<s.length(); i++){
      ret[(i+shift)%s.length()] = s.charAt(i);
    }
    return new String(ret);
  }

  /**
  * returns keyWord based on given Secret text and its keyLength;
  * uses CaesarShift to find offset
  */
  private static String getKeyWord(String secret, int keyLength){
    String ret = "";
    String[] arr = new String[keyLength];
    for(int i = 0; i<arr.length; i++){
      arr[i] = "";
    }
    for(int i = 0; i<keyLength; i++){
      for(int j = i; j<secret.length(); j += keyLength){
        arr[i] += secret.charAt(j);
      }
    }
    for(String s : arr){
      ret += (char)(getCeaserShift(s)+'A');
    }
    return ret;
  }

  /**
  * finds amount if Shift used by Caesar Cipher for a given String
  */
  private static int getCeaserShift(String in){
    int[] lettercounts = new int[26];
    for(int i = 0; i < lettercounts.length; i++){
      lettercounts[i] = 0;
    }
    //count letters
    for(char c : in.toCharArray()){
      lettercounts[c -'A']++;
    }

    //get highest
    int maxPos = 0;
    int maxVal = 0;
    for(int i = 0; i < lettercounts.length; i++){
      if(lettercounts[i] > maxVal){
        maxPos = i;
        maxVal = lettercounts[i];
      }
    }
    return ((maxPos+'A'-'E') > 0 ? (maxPos+'A'-'E') : (maxPos+'A'-'E')+26);
  }


}
