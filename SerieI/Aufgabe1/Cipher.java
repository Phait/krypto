package Aufg1;

import java.util.LinkedList;

public class Cipher{

    private static char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    /**
    * encrypt given String using given keyword and keyletter
    */
    public static String encrypt(String in, String keyword, char keyletter){
        char[] secretalphabet = getSecretAlphabet(keyword, keyletter);
        char[] ret = cleanString(in).toCharArray();
        for(int i = 0; i < ret.length; i++) ret[i] = secretalphabet[ret[i]-65];
        return new String(ret);
    }

    /**
    * decrypt given String using given keyword and keyletter
    */
    public static String decrypt(String in, String keyword, char keyletter){
        char[] secretalphabet = getSecretAlphabet(keyword, keyletter);
        char[] ret = in.toCharArray();
        LinkedList<Character> tmp = new LinkedList<Character>();
        for(int i = 0; i < secretalphabet.length; i++){//secretalphabet to List
            tmp.add(secretalphabet[i]);
        }
        for(int i = 0; i < ret.length; i++) ret[i] = alphabet[tmp.indexOf(ret[i])];
        return new String(ret);
    }

    /**
    * generate Alphabet based on keyword and letter; Used for switching of original Letters
    */
    private static char[] getSecretAlphabet(String keyword, char keyletter){
        LinkedList<Character> tmp = new LinkedList<Character>();
        for(char c : keyword.toUpperCase().toCharArray()){//Add keyword
            tmp.add(c);
        }
        for(char c : alphabet){//add entire alphabet
            tmp.add(c);
        }
        for(int i = 0; i < tmp.size(); i++){//remove multiple entries
            for (int j = i+1; j < tmp.size(); j++){
                if(tmp.get(i) == tmp.get(j)){
                    tmp.remove(j);
                    j--;
                }
            }
        }
        char[] ret = new char[tmp.size()];
        for(int i = 0; i < tmp.size(); i++) ret[(i+keyletter-65)%26] = tmp.get(i);
        return ret;
    }
    /**
    * change Umlauts for alternative representation and remove anything besides letters
    */
    private static String cleanString(String in){
        return  in.toUpperCase()
        .replaceAll("Ä","AE")
        .replaceAll("Ö","OE")
        .replaceAll("Ü","UE")
        .replaceAll("ß","SS")
        .replaceAll("[\\W\\d]","");
    }


    public static void main(String[] args){
        if(args.length != 4){
            System.err.println("Wrong number of arguments <1:encrypt/0:decrypt> <Text> <secret> <startletter>");
            System.exit(1);
        }
        try{
            if(Integer.parseInt(args[0]) == 1)
                System.out.println(Cipher.encrypt(args[1],args[2],args[3].toCharArray()[0]));
            if(Integer.parseInt(args[0]) == 0)
                System.out.println(Cipher.decrypt(args[1],args[2],args[3].toCharArray()[0]));
        }catch(Exception e){
            System.err.println("Wrong number");
            System.exit(1);
        }


/*
        System.out.println(Cipher.encrypt("Dies ist ein test ÖÄÜ!!","GEHEIMSCHRIFT",'U'));
        System.out.println(Cipher.decrypt("TKAYKYZAKQZAYZUACAGA","GEHEIMSCHRIFT",'U'));
*/   }
}
