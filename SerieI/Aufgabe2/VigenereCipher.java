/**
 * @author Sonja Doerband, Veith Guentel, Florian Loehden
 */

public class VigenereCipher {
    public static void main(String[] args) {
        if(args.length != 3){
            System.err.println("Wrong Parameters <key> <text>");
            System.exit(1);
        }

        if(args.length != 3){
            System.err.println("Wrong number of arguments <1:encrypt/0:decrypt> <key> <text>");
            System.exit(1);
        }
        try{
            if(Integer.parseInt(args[0]) == 1)
                System.out.println(VigenereCipher.encrypt(args[2],args[1]));
            if(Integer.parseInt(args[0]) == 0)
                System.out.println(VigenereCipher.decrypt(args[2],args[1]));
        }catch(Exception e){
            System.err.println("Wrong number");
            System.exit(1);
        }
    }

    /**
     *  encrytes a text with a key in vignere
     * @param text the text to encrypt
     * @param key the key to encrypt
     * @return the encrypted text
     */
    static String encrypt(String text, final String key) {
        String res = "";
        text = text.toUpperCase();
        for (int i = 0, j = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c < 'A' || c > 'Z') continue;
            res += (char)((c + key.charAt(j) - 2 * 'A') % 26 + 'A');
            j = ++j % key.length();
        }
        return res;
    }

    /**
     * decrytes a text with a key in vignere
     * @param text the text to decrypt
     * @param key the key to decrypt
     * @return the decrypted text
     */
    static String decrypt(String text, final String key) {
        String res = "";
        text = text.toUpperCase();
        for (int i = 0, j = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c < 'A' || c > 'Z') continue;
            res += (char)((c - key.charAt(j) + 26) % 26 + 'A');
            j = ++j % key.length();
        }
        return res;
    }
}