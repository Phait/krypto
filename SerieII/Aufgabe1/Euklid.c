#include <stdio.h>

/**
 * Findet den größten gemeinsamen Teiler(ggt)
 * b wird 0 wenn es durch a ohne rest teilbar ist
 * @param a
 * @param b
 * @return größter gemeinsamer Teiler der Beiden Zahlen
 */
int euklid(int a, int b){
    if(b == 0) return a;
    else return euklid(b, a%b);
}

int main() {
    int a = 78;
    int b = 52;
    int c = euklid(a,b);

    printf("a: %d b: %d ggT: %d\n", a,b,c);
    printf("Press Any Key to Continue\n");
    getchar();
    return 0;
}
_