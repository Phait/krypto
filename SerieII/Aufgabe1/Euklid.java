
public class Euklid{
    public static void main(String[] args){
        int a = 78;
        int b = 52;

        System.out.println("a: " + a + " b: " + b + " ggT: " + Euklid.algEuklid(a,b));
    }

    /**
     * Findet den größten gemeinsamen Teiler(ggt)
     * b wird 0 wenn es durch a ohne rest teilbar ist
     * @param a
     * @param b
     * @return größter gemeinsamer Teiler der Beiden Zahlen
     */
    public static int algEuklid(int a, int b){
        if(b==0) return a;
        else return algEuklid(b,a%b);
    }
}