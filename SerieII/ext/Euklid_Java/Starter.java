/**
 * Created by Christian on 04.01.2016.
 */
public class Starter {

    public static void main(String args[]){
        //System.out.println((new Euklid(13,45)).useClassic());
        int a = 78;
        int b = 52;
        int[] test = (new Euklid()).extendedEuclid(a,b);

        System.out.println(test[0]+" = "+test[1]+"*"+a+" + "+test[2]+"*"+b);
    }
}
