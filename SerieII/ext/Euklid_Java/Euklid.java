/**
 * Created by Christian on 04.01.2016.
 */
public class Euklid {

    /**
     * Führt den Eklid'schen Algorithmus mit wiederholter Subtraktion durch
     * @param arga erste natürliche Zahl
     * @param argb zweite natürliche Zahl
     * @return ggT der beiden Zahlen
     */
    public int useClassic(int arga, int argb){
        if(arga == 0)
            return argb;

        int b = argb;
        int a = arga;

        while(b!=0){
            if(a > b)
                a = a-b;
            else
                b = b - a;
        }
        return a;
    }

    /**
     * Führt den Euklid'schen Algorithmus mit Moduloberechnung durch
     * Zusützlich wird die Zahlenkombination der einzelnen Schritte ausgegeben
     * @param arga erste natürliche Zahl
     * @param argb zweite natürliche Zahl
     * @return ggT der beiden Zahlen
     */
    public int useModulo(int arga, int argb){
        StringBuilder sb = new StringBuilder();
        int a, b;

        /** oBdA: a>b */
        a = (arga>argb)?arga:argb;
        b = (arga>argb)?argb:arga;

        /** Euklid'scher Algorithmus mit Modulorechnung */
        while(b != 0){
            int h = a % b;

            /** Ausgabe */
            sb.append(a + " = "+ a/b + " * " + b + " + " + h +" \n");

            a = b;
            b = h;
        }

        System.out.println("GGT ist: "+ a + "\n"+sb.toString());
        return a;
    }

    /** Rekursive Berechnung der Linearkombination von a und b mit euklidschem Algorithmus */
    int[] extendedEuclid(int a, int b){
        if(b == 0){
            return new int[]{a, 1, 0};
        }
        int[] retVal = extendedEuclid(b, a%b);
        return new int[]{retVal[0],retVal[2],retVal[1] - (a / b)*retVal[2]};
    }


 }


